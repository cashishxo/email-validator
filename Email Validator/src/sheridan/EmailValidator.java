package sheridan;

import java.util.regex.Pattern;

public class EmailValidator {
	public static boolean isEmailFormatValid(String email) {
		return Pattern.compile("\\s*\\@\\s*[.]*").matcher(email).find();
	}

	public static boolean isEmailContainsOneAt(String email) {
		return Pattern.compile("\\@").matcher(email).find();
	}
	
	public static boolean isEmailAccountValid(String email) {
		return Pattern.compile("^[a-zA-Z]{3,}\\@[.]*[.]*[.]*").matcher(email).find();	
	}
	public static boolean isEmailDomainValid(String email) {
		return Pattern.compile("[.]*\\@\\w{3,}[.]*").matcher(email).find();
	}
}
