package sheridan;

import static org.junit.Assert.*;
import sheridan.EmailValidator;
import org.junit.Test;
public class EmailValidatorTest {
	@Test
	public void isEmailFormatValidRegular() {
		String email = "kashish@gmai.com";
		assertTrue("The test case passed", EmailValidator.isEmailFormatValid(email));
	}

	@Test
	public void isEmailFormatValidException() {
		String email = "kashishgmailcom";
		assertFalse("The test case passed", EmailValidator.isEmailFormatValid(email));
		
	}
	@Test 
	public void isEmailFormatValidBoundaryIn() {
		String email = "k@g.c";
		assertTrue("the test case passed", EmailValidator.isEmailFormatValid(email));
	}
	@Test
	public void isEmailFormatValidBoundaryOut() {
		String email = "kg.c";
		assertFalse("the test case passed", EmailValidator.isEmailFormatValid(email));	
	}
	
	@Test
	public void isEmailContainsOneAtRegular() {
		String email = "kashish@gmail.com";
		assertTrue("The test case passed", EmailValidator.isEmailContainsOneAt(email));
	}

	@Test
	public void isEmailContainsOneAtException() {
		String email = "dfadfa@@@";
		assertTrue("The test case failed", EmailValidator.isEmailContainsOneAt(email));
	}
	@Test
	public void isEmailContainsOneBoundaryOut() {
		String email = "dafda@@";
		assertTrue("The test case failed", EmailValidator.isEmailContainsOneAt(email));
	}
	@Test
	public void isEmailContainsOneBoundaryIn() {
		String email = "@";
		assertTrue("The test case passed", EmailValidator.isEmailContainsOneAt(email));
	}
	@Test
	public void isEmailAccountValidRegular() {
		String email="kass@k.c";
		assertTrue("The test case passed", EmailValidator.isEmailAccountValid(email));
	}
	@Test
	public void isEmailAccountValidException() {
		String email="k@k.c";
		assertFalse("The test case Failed", EmailValidator.isEmailAccountValid(email));
	}
	@Test
	public void isEmailAccountValidBoundaryIn() {
		String email="kss@k.c";
		assertTrue("The test case passed", EmailValidator.isEmailAccountValid(email));
	}
	@Test
	public void isEmailAccountValidBoundaryOut() {
		String email="ss@k.c";
		assertFalse("The test case failed", EmailValidator.isEmailAccountValid(email));
	}
	@Test
	public void isEmailDomainValidRegular() {
		String email ="kas@gmail.com";
		assertTrue("The test case passed", EmailValidator.isEmailDomainValid(email));
	}
	@Test
	public void isEmailDomainValidException() {
		String email ="kas@g.com";
		assertFalse("The test case passed", EmailValidator.isEmailDomainValid(email));
	}
	@Test
	public void isEmailDomainValidBoundaryIn() {
		String email = "kas@gma.com";
		assertTrue("the test case passed", EmailValidator.isEmailDomainValid(email));
	}
	@Test
	public void isEmailDomainValidBoundaryOut() {
		String email = "kas@gm.com";
		assertFalse("the test case failed", EmailValidator.isEmailDomainValid(email));
	}
}
